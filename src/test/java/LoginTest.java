import com.upay.alpha.Utility;
import com.upay.alpha.page.LoginPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;

/**
 * Created by justiceg on 19/04/2017.
 */
public class LoginTest extends LoginPage {

    Utility utility = new Utility();

    @BeforeMethod
    public void openWebDriver() {
        utility.initializeBrowser();
        setDriver(Utility.driver);
    }

    @AfterMethod
    public void closeWebDriver() {
        utility.closeDiver();
    }

    @Test(priority = 2)
    public void test_SignInValidInformation() throws Exception {
        Assert.assertTrue(loginSucess("justice@upay.lk", "Upay@123"), "lgin_01");
    }

    @Test(priority = 1)
    public void test_ExistingUserSignInWithInvalidInformation() throws Exception {
        Assert.assertTrue(loginFail("justice@upay.lk", "Upay@124","User not authonticated"), "lgin_02");
    }

    @Test(priority = 1)
    public void test_NewUserSignInWithInvalidInformation() throws Exception {
        Assert.assertTrue(loginFail("scasc@uscasy.lk", "Uwdqwd@qd34","User not authonticated"), "lgin_03");
    }

    @Test(priority = 4)
    public void testAddCard() {
        String a = ("demoReportPass");
        Assert.assertTrue(true);
    }





}
