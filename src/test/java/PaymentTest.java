import com.upay.alpha.Utility;
import com.upay.alpha.page.LoginPage;
import com.upay.alpha.page.PaymentPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Created by maheshchathuranga on 5/25/17.
 */
public class PaymentTest extends PaymentPage {

    Utility utility = new Utility();
    LoginPage loginPage = new LoginPage();

    @BeforeTest
    public void openWebDriver() {
        utility.initializeBrowser();
        loginPage.setDriver(Utility.driver);
        setDriver(Utility.driver);

    }


    @AfterTest
    public void closeWebDriver() {
        utility.closeDiver();
    }

    @Test(priority = 4)
    public void testTransaction() throws Exception {

        Assert.assertTrue(loginPage.loginSucess("justice@upay.lk", "Upay@123")
                && payment("Softlogic Life Insurance", "1234567890", "100"), "testTransaction");

    }

}
