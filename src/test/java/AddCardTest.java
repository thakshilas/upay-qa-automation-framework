import com.upay.alpha.Utility;
import com.upay.alpha.page.AddCardPage;
import com.upay.alpha.page.LoginPage;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Created by USER on 5/26/2017.
 */
public class AddCardTest extends AddCardPage {

    Utility utility = new Utility();
    LoginPage loginPage = new LoginPage();

    @BeforeTest
    public void openWebDriver(){
        utility.initializeBrowser();
        loginPage.setDriver(Utility.driver);
        setDriver(Utility.driver);
    }

    @AfterTest
    public void closeWebDriver(){
        utility.closeDiver();
    }

    @Test
    public void testAddCard() throws Exception{
        Assert.assertTrue(loginPage.loginSucess("justice@upay.lk", "Upay@123")&& NewUserAddCardSucess("justice","123124124124","123","10/10"), "testAddcard");
    }
}
