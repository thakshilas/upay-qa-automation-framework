package com.upay.alpha.page;

import com.upay.alpha.PropertiesCache;
import com.upay.alpha.Utility;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by justiceg on 5/26/2017.
 */
public class SignUpPage {

    String SignUp = PropertiesCache.getInstance().getProperty("Sign.Up");
    String SignUp_Name = PropertiesCache.getInstance().getProperty("SignUp.Name");
    String SignUp_Email = PropertiesCache.getInstance().getProperty("SignUp.Email");
    String SignUp_NIC = PropertiesCache.getInstance().getProperty("SignUp.NIC");
    String SignUp_MobileNo = PropertiesCache.getInstance().getProperty("SignUp.MobileNo");
    String SignUp_Password = PropertiesCache.getInstance().getProperty("SignUp.Password");
    String SignUp_ConfirmPassword = PropertiesCache.getInstance().getProperty("SignUp.ConfirmPassword");
    String Register = PropertiesCache.getInstance().getProperty("Register");


    private WebDriver driver;
    private Utility utility = new Utility();


    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    protected void SignUpSucess(String Name, String Email_Address, String NIC, String Mobile_N0, String Password, String Confirm_Password) {

        utility.waitUntilDisplay(SignUp);

        driver.findElement(By.id(SignUp)).click();

        utility.waitUntilDisplay(SignUp_Name);

        driver.findElement(By.id(SignUp_Name)).sendKeys(Name);
        driver.findElement(By.id(SignUp_Email)).sendKeys(Email_Address);
        driver.findElement(By.id(SignUp_NIC)).sendKeys(NIC);
        driver.findElement(By.id(SignUp_MobileNo)).sendKeys(Mobile_N0);
        driver.findElement(By.id(SignUp_Password)).sendKeys(Password);
        driver.findElement(By.id(SignUp_ConfirmPassword)).sendKeys(Confirm_Password);

        driver.findElement(By.id(Register)).click();

    }

}
