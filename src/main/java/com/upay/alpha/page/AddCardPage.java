package com.upay.alpha.page;

import com.upay.alpha.PropertiesCache;
import com.upay.alpha.Utility;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by USER on 5/26/2017.
 */
public class AddCardPage {

    String Add_Card_Quick_Link = PropertiesCache.getInstance().getProperty("Card.Add.Quick.Action");
    String Card_Holder_Name = PropertiesCache.getInstance().getProperty("Card.Holder.Name");
    String Card_Number = PropertiesCache.getInstance().getProperty("Card.number");
    String Card_CVV = PropertiesCache.getInstance().getProperty("Card.cvv");
    String Card_Expiry = PropertiesCache.getInstance().getProperty("Card.Expiry");

    String Card_Submit = PropertiesCache.getInstance().getProperty("Card.Submit");


    private WebDriver driver;
    private Utility utility = new Utility();


    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    protected boolean NewUserAddCardSucess(String Name, String CardNumber, String CVV, String Expiry){

        utility.waitUntilDisplay(Add_Card_Quick_Link);
        driver.findElement(By.id(Add_Card_Quick_Link)).click();
        utility.waitUntilDisplay(Card_Holder_Name);
        driver.findElement(By.className(Card_Submit)).click();

        return true;

    }

    protected void AddCardSucess(String Name, String CardNumber, String CVV, String Expiry) {

    }

}
