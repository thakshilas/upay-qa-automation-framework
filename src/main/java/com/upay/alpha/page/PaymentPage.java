package com.upay.alpha.page;

import com.upay.alpha.PropertiesCache;
import com.upay.alpha.Utility;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static java.lang.Thread.sleep;

/**
 * Created by maheshchathuranga on 5/25/17.
 */
public class PaymentPage {

    private static String Make_Payment_Service = PropertiesCache.getInstance().getProperty("MakePayment.Merchant.Service");
    private static String Make_Payment_Service_List = PropertiesCache.getInstance().getProperty("MakePayment.Merchant.Service.List");
    private static String Make_Payment_Account = PropertiesCache.getInstance().getProperty("MakePayment.Merchant.Account");
    private static String Make_Payment_Amount = PropertiesCache.getInstance().getProperty("MakePayment.Merchant.Amount");
    private static String Make_Payment_Method = PropertiesCache.getInstance().getProperty("MakePayment.Merchant.Payment.Method");
    private static String Make_Payment_Submit = PropertiesCache.getInstance().getProperty("MakePayment.Merchant.Submit");
    private static String Make_Payment_Confirm = PropertiesCache.getInstance().getProperty("MakePayment.Merchant.Confirm");
    private static String Dashboard_Side_Panel_Make_Payment = PropertiesCache.getInstance().getProperty("Dashboard.Side.Panel.MakePayment");
    private WebDriver driver;
    private Utility utility = new Utility();


    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public boolean payment(String paymentService, String customerAccount, String amount) throws InterruptedException {
        utility.waitUntilDisplay(Dashboard_Side_Panel_Make_Payment);
        driver.findElement(By.id(Dashboard_Side_Panel_Make_Payment)).click();
        utility.waitUntilDisplay(Make_Payment_Service);
        driver.findElement(By.id(Make_Payment_Service)).sendKeys(paymentService);
        utility.waitUntilDisplay(Make_Payment_Account);
        driver.findElement(By.id(Make_Payment_Account)).sendKeys(customerAccount);
        utility.waitUntilDisplay(Make_Payment_Amount);
        driver.findElement(By.id(Make_Payment_Amount)).sendKeys(amount);
        utility.waitUntilDisplay(Make_Payment_Submit);
        driver.findElement(By.id(Make_Payment_Submit)).click();
        utility.waitUntilDisplay(Make_Payment_Confirm);
        driver.findElement(By.id(Make_Payment_Confirm)).click();
        sleep(6000);
        return true;
    }
}
