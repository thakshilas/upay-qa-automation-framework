package com.upay.alpha.page;

import com.upay.alpha.PropertiesCache;
import com.upay.alpha.Utility;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


/**
 * Created by maheshchathuranga on 5/23/17.
 */
public class LoginPage {

    private static String Login_email = PropertiesCache.getInstance().getProperty("Login.Email");
    private static String Login_password = PropertiesCache.getInstance().getProperty("Login.Password");
    private static String Login_button = PropertiesCache.getInstance().getProperty("Login.Button");
    private static String Dashboard_myCarousel = PropertiesCache.getInstance().getProperty("Dashboard.Carousel");
    private static String Toast_container = PropertiesCache.getInstance().getProperty("Toast.container");

    private WebDriver driver;
    private Utility utility = new Utility();

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public boolean loginSucess(String Email, String Password) throws InterruptedException {

        utility.waitUntilDisplay(Login_email);
        driver.findElement(By.id(Login_email)).sendKeys(Email);
        driver.findElement(By.id(Login_password)).sendKeys(Password);
        driver.findElement(By.id(Login_button)).click();

        utility.waitUntilDisplay(Dashboard_myCarousel);
        if (driver.findElement(By.id(Dashboard_myCarousel)).isDisplayed()) {
            return true;
        }
        return false;
    }

    public boolean loginFail(String Email, String Password, String ExpectedError) throws InterruptedException {

        utility.waitUntilDisplay(Login_email);
        driver.findElement(By.id(Login_email)).sendKeys(Email);
        driver.findElement(By.id(Login_password)).sendKeys(Password);
        driver.findElement(By.id(Login_button)).click();

        utility.waitUntilDisplay(Toast_container);

        if(driver.findElement(By.id(Toast_container)).getText().contains(ExpectedError)){
            System.out.println(driver.findElement(By.id(Toast_container)).getText());
            return true;
        }
        return false;
    }
}
