package com.upay.alpha;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by justiceg on 19/04/2017.
 */
public class Utility {

    String browserType = PropertiesCache.getInstance().getProperty("browser.type");
    String FE_URL = PropertiesCache.getInstance().getProperty("FE.URL");

    String DeviceName = PropertiesCache.getInstance().getProperty("Device.Name");


    String Dashboard_Side_Panel_Make_Payment = PropertiesCache.getInstance().getProperty("Dashboard.Side.Panel.MakePayment");

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static WebDriver driver;

    public void initializeBrowser() {

        if (browserType.equalsIgnoreCase("Firefox")) {
            String fireFoxFilePath = System.getProperty("user.dir") + File.separator + "webdriver" +
                    File.separator + "geckodriver.exe";
            System.setProperty("webdriver.gecko.driver", fireFoxFilePath);
            FirefoxOptions options = new FirefoxOptions();
            options.addArguments("--start-maximized");
            driver=new FirefoxDriver();
            driver.get(FE_URL);
            System.out.println("+++++++++ Opening Firefox Browser +++++++\n");
            driver.manage().window().maximize();

        } else if (browserType.equalsIgnoreCase("Chrome")) {
            String chromeFilePath = System.getProperty("user.dir") + File.separator + "webdriver" +
                    File.separator + "chromedriver.exe";
            System.setProperty("webdriver.chrome.driver", chromeFilePath);
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            driver=new ChromeDriver(options);
            driver.get(FE_URL);
            driver.manage().window().maximize();
            System.out.println("+++++++++ Opening Chrome Browser ++++++++");

        } else if (browserType.equalsIgnoreCase("Mobile")) {
            Map<String, String> mobileEmulation = new HashMap<String, String>();
            mobileEmulation.put("deviceName", DeviceName);
            Map<String, Object> chromeOptions = new HashMap<String, Object>();
            chromeOptions.put("mobileEmulation", mobileEmulation);

            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
            String chromeFilePath = System.getProperty("user.dir") + File.separator + "webdriver" +
                    File.separator + "chromedriver.exe";
            System.setProperty("webdriver.chrome.driver", chromeFilePath);
            driver=new ChromeDriver(capabilities);
            driver.get(FE_URL);
            System.out.println("+++++++++ Opening Chrome Browser ++++++++");

        } else if (browserType.equalsIgnoreCase("IE")) {
            String ieFilePath = System.getProperty("user.dir") + File.separator + "webdriver" +
                    File.separator + "IEDriverServer.exe";
            System.setProperty("webdriver.ie.driver", ieFilePath);
            DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
            caps.setCapability("ignoreZoomSetting", true);
            caps.setCapability("nativeEvents", false);
            driver=new InternetExplorerDriver(caps);
            driver.get(FE_URL);
            driver.manage().window().maximize();
            System.out.println("+++++++++ Opening IE Browser +++++++++");

        } else if (browserType.equalsIgnoreCase("Safari")) {
            String ieFilePath = System.getProperty("user.dir") + File.separator + "webdriver" +
                    File.separator + "SafariDriver.safariextz";
            System.setProperty("webdriver.ie.driver", ieFilePath);
            DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
            caps.setCapability("ignoreZoomSetting", true);
            caps.setCapability("nativeEvents", false);
            driver=new InternetExplorerDriver(caps);
            driver.get(FE_URL);
            driver.manage().window().maximize();
            System.out.println("+++++++++ Opening Safari Browser +++++++++");
        }

    }

    public void waitUntilDisplay(String webElementName) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.or(ExpectedConditions.visibilityOfElementLocated(By.id(webElementName))));
    }

    public void closeDiver() {
        driver.quit();
    }
}
