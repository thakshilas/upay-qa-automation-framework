# README #

### This Repository is for versioning and maintaining the Upay QA Automation Framework ###

### How do I get set up? ###

* Clone the develop branch to your local machine and do all changes on the develop branch itself.

### Contribution guidelines ###

* all methods and classes that are created will be created on the develop branch
* Code that is committed to the develop branch will be reviewed and then merged to the master branch.

### Who do I talk to? ###

* For any clarification please feel free to speak to Justice about them.
* Or one of the QA Automation team members.